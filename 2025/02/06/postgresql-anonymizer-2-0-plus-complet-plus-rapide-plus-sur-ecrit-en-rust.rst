.. index::
   ! PostgreSQL Anonymizer 2.0  

.. _anonymizer_2_0_0:

======================================================================================================================
2025-02-06 **PostgreSQL Anonymizer 2.0 - Plus Complet, Plus Rapide, Plus Sûr + Une Réécriture Complète en Rust**
======================================================================================================================

- https://blog.dalibo.com/2025/02/06/postgresql_anonymizer_2.html
- https://gitlab.com/dalibo/postgresql_anonymizer/-/blob/latest/NEWS.md


PostgreSQL Anonymizer 2.0 
==============================

Nous sommes ravis d’annoncer la sortie de PostgreSQL Anonymizer 2.0, une
avancée majeure dans le domaine de la protection de la confidentialité des
bases de données.

Cette extension d’anonymisation de données propose désormais cinq stratégies
complémentaires : 

- Masquage Dynamique, 
- Masquage Statique, 
- Dumps Anonymes, 
- Vues de Masquage, 
- et Wrappers de Masquage de Données.

Chaque stratégie est complétée par une suite de fonctions de masquage incluant
des techniques avancées telles que : 

- substitution, 
- randomisation, 
- génération de données factices, 
- pseudonymisation, 
- brouillage partiel, 
- brassage, 
- bruitage
- et généralisation.  

**Une Réécriture Complète en Rust**
=======================================

- https://github.com/pgcentralfoundation/pgrx

La version 2.0 représente une transformation fondamentale de notre base de
code, entièrement réécrite en Rust en utilisant le framework révolutionnaire
`PGRX <https://github.com/pgcentralfoundation/pgrx>`_. 

**Ce changement stratégique apporte des améliorations exceptionnelles en
termes de sécurité mémoire, d’efficacité des ressources, de tests et de
maintenabilité du code**.

Par ailleurs, cette version majeure introduit des fonctionnalités puissantes
qui étendent significativement votre boîte à outils de protection des données :

- Générateur de fausses données avancé avec un réalisme amélioré Masquage
  dynamique transparent basé sur les rôles Politiques de masquage multiples
- Exports anonymisés transparents avec pg_dump Paquets Debian

Pour aider les utilisateurs à démarrer rapidement, nous publions un tutoriel
complet qui vous guidera à travers des scénarios pratiques d’anonymisation.

Voir https://postgresql-anonymizer.readthedocs.io/en/stable/tutorials/0-intro/

Pour les utilisateurs existants, nous avons assuré une transition en douceur
depuis la version 1.x. Consultez notre guide d’UPGRADE complet pour un processus
de migration simple.  Remerciements

Cette version inclut du code, des corrections de bugs, de la documentation,
des revues de code et des idées de Giampaolo Capelli, José Pedro Saraiva,
Guillaume Risbourg, Austin Putman, Carlos Ruiz, Thibaut Madeleine, Konrad
Kucharski, Leo Long, Ben Dempsey, Pierre-Marie Petit, Danilo Lourenço Costa
Oliveira et de nombreux autres contributeurs.

Et aussi un merci spécial à l’équipe PGRX pour leur travail incroyable !


Comment contribuer ?
========================

PostgreSQL Anonymizer fait partie de l’initiative Dalibo Labs. 
Il est principalement développé par Damien Clochard.

C’est un projet ouvert, les contributions sont les bienvenues. 

Nous avons besoin de vos retours et de vos idées ! 

Faites-nous savoir ce que vous pensez de cet outil, comment il répond à vos 
besoins et quelles fonctionnalités manquent.

Si vous voulez aider, vous pouvez trouver une liste de Junior Jobs ici :


- https://gitlab.com/dalibo/postgresql_anonymizer/issues?label_name%5B%5D=Junior+Jobs


Enquête PostgreSQL Anonymizer 2025
==========================================

Nous lançons une enquête rapide pour en apprendre davantage sur nos
utilisateurs. 

Prenez quelques minutes, cliquez sur le lien ci-dessous et répondez
à 10 questions !

https://dali.bo/anon_survey_2025
