

.. _postgres_weekly_586:

========================================================================
2025-01-30 Postgres weekly 586, Full Text Search, LOGON trigger PG17
========================================================================

- https://postgresweekly.com/issues/586

A New Postgres Block Storage Layout for Full Text Search
==============================================================

- https://www.paradedb.com/blog/block_storage_part_one

We recently completed one of our biggest engineering bets to date: migrating pg_search, 
a Postgres extension for full text search and analytics, to Postgres' block storage system. 

In doing so, pg_search is the first-ever extension1 to port an external file 
format to Postgres block storage.


Logon trigger in PostgreSQL
=================================

Starting with version 17, PostgreSQL provides a **feature many Oracle users 
have been waiting for: the LOGON trigger**. 

**The idea is to make the database launch a function as soon as the user tries to log in**.
