

=========================
2023-07
=========================


.. _dba_day_2023:

2023-07-27 **Dalibo DBA Appreciation Day : la parole aux DBA !**
=======================================================================

- https://blog.dalibo.com/2023/07/27/dba_appreciation_day.html
- DBA Appreciation Day : la parole aux DBA !
- https://fosstodon.org/@fljdin
- https://fosstodon.org/@fljdin.rss


Publié le 27/07/2023 par Laura Ricci, Benoit Lobréau, Florent Jardin,
Guillaume Lelarge, Christophe Courtois, Alain Lesage

Saint-Étienne, le 27 juillet 2023

Ayant découvert par hasard le DBA Appreciation Day, j’en ai profité pour
interroger mes collègues DBA sur leur métier.

Une bouteille à la mer lancée en interne, cinq réponses reçues !
Les voici “brutes”.


Qu’aimes-tu le plus, dans ton métier ?
----------------------------------------

BENOIT : « Ce que j’apprécie le plus dans ce métier, c’est la diversité
des contextes clients que l’on rencontre.
Cela nous permet de voir beaucoup de cas d’utilisation de PostgreSQL. »

FLORENT : « L’adaptation requise pour chaque mission me force à innover
et imaginer les solutions les plus appropriées aux besoins du client.
On ne s’ennuie jamais ! »

GUILLAUME : « Aider les clients, apprendre et découvrir. »

CHRISTOPHE : « Faire revenir à la vie un serveur catatonique juste en
ajoutant un index. (ça me rappelle un billet de blog que je dois te faire) »

ALAIN : « De très loin, faire gagner du temps à nos clients.
Durant un audit, nous avons pu avec les bons index faire économiser 6h
de traitements par jour à l’un d’entre eux. Gagner quelques centièmes
de seconde par exécution sur des requêtes répétées des milliers de fois
dans la journée a eu un impact énorme ! »

Quel est actuellement le principal défi des DBA ?
-----------------------------------------------------

BENOIT : « Pour moi, le principal défi des DBA est de s’adapter à
l’environnement changeant des bases de données. Non seulement les
technologies sur lesquelles les bases de données sont installées
évoluent, mais aussi celles qui les utilisent, tout comme les autres
technologies de stockage de l’information. »

FLORENT : « L’éducation et le partage des connaissances sont primordiaux,
l’avis d’un expert DBA est rarement sollicité par les équipes de
développement ou les architectes, alors que de nombreux problèmes de
performance et de modélisation pourraient être évités s’ils étaient
identifiés durant toute la vie d’un projet. »

GUILLAUME : « Rester pointu en permanence. »

CHRISTOPHE : « Réussir à comprendre suffisamment chaque élément de la
grande et mouvante chaîne technico-économico-fonctionnelle qui va des
caractéristiques des disques aux utilisateurs finaux, via les derniers
outils et architectures à la mode, les spécificités de chaque OS ou cloud,
et le code de PostgreSQL, pour trouver pourquoi ce fichu serveur rame. »

ALAIN : « Le défi actuel selon moi est de continuer d’expliquer à nos
clients que la base de données n’est pas un produit basique, interchangeable,
sans valeur ajoutée, et que des solutions “clé en main” proposées par certains
vendeurs - par exemple, le cloud - viennent forcément avec des compromis.
S’il n’est pas possible de faire du sur-mesure, certaines optimisations
faisant gagner beaucoup de temps peuvent être impossibles à mettre en place ! »

Ajoutons qu’un DBA taquin dont nous tairons le prénom a répondu :
« Comprendre les développeurs. Convaincre les développeurs. 😄 »

Comment vois-tu évoluer ton métier ?
------------------------------------------

FLORENT : « Le métier a déjà beaucoup évolué depuis vingt ans et s’est
complétement transformé pour certain·es avec l’émergence des plateformes
Cloud et l’automatisation qui en a découlé.
Mais fondamentalement, tant qu’il y aura des bases de données, le DBA
restera toujours celui qui murmure à leurs oreilles. »

GUILLAUME : « J’espère qu’il n’évoluera pas. Un peu de constance, ça
fait du bien. Et puis, l’essentiel, c’est que nos connaissances évoluent,
pas le métier en lui-même. »

ALAIN : « Comme mes collègues, je souhaite que nos clients soient bien
formés, et connaissent le potentiel des outils qu’ils utilisent.
Il est important pour eux de savoir comment bien les utiliser, et il est
important pour nous de rester experts et de savoir comment ils fonctionnent.

Dans le cas de PostgreSQL, le modèle de gestion de la concurrence MVCC
est essentiel à comprendre, et bien connaître ce fonctionnement permet
de prendre les bonne décisions en termes d’architecture tant pour les
applications au-dessus de la base de données, que pour le matériel la
faisant fonctionner. »


.. note:: The DBA Day 2024 will be the 5th July 2024


2023-04-24 **sqlite-utils now supports plugins**
======================================================

- https://sqlite-utils.datasette.io/en/stable/plugins.html
- https://sqlite-utils.datasette.io/en/stable/changelog.html#v3-34
- https://sqlite-utils.datasette.io/en/stable/plugins.html#plugins


`sqlite-utils 3.34 <https://sqlite-utils.datasette.io/en/stable/changelog.html#v3-34>`_
is out with a major new feature: support for plugins.

sqlite-utils is my combination Python library and command-line tool for
manipulating SQLite databases.

It recently celebrated its fifth birthday, and has had over 100 releases
since it first launched back in 2018.

The new plugin system is inspired by similar mechanisms in Datasette
and LLM.
It lets developers add new features to sqlite-utils without needing to
get their changes accepted by the core project.

I love plugin systems. As an open source maintainer they are by far the
best way to encourage people to contribute to my projects
I can genuinely wake up in the morning and my software has new features,
and I didn't even need to review a pull request.

Plugins also offer a fantastic medium for exploration and experimentation.
I can try out new ideas without committing to supporting them in core,
and without needing to tie improvements to them to the core release cycle.

Version 3.34 adds two initial plugin hooks: register_commands() and
prepare_connection(). These are both based on the equivalent hooks in Datasette.



2023-07-24 PostgreSQL Hebdo #103 par Sebastien Lardiere
==============================================================

- http://sebastien.lardiere.net/blog/index.php/post/2023/07/21/PostgreSQL-Hebdo-103
- https://dataegret.com/2023/06/please-welcome-pg_index_watch-a-utility-for-dealing-with-index-bloat-on-frequently-updated-tables/
- https://dataegret.com/2023/07/automated-index-bloat-management-how-pg_index_watch-keeps-postgresql-indexes-lean/
- https://kmoppel.github.io/2023-07-04-til-in-is-not-the-same-as-any/
- https://www.cybertec-postgresql.com/en/subqueries-and-performance-in-postgresql
- https://www.citusdata.com/blog/2023/07/18/citus-12-schema-based-sharding-for-postgres/
- https://www.percona.com/blog/how-to-measure-the-network-impact-on-postgresql-performance/

Lu ces dernières semaines :

- Please welcome `Pg_index_watch <https://dataegret.com/2023/06/please-welcome-pg_index_watch-a-utility-for-dealing-with-index-bloat-on-frequently-updated-tables/>`_
  a utility for dealing with index bloat on frequently updated tables
  puis `Automated index bloat management <https://dataegret.com/2023/07/automated-index-bloat-management-how-pg_index_watch-keeps-postgresql-indexes-lean/>`_
  How pg_index_watch keeps PostgreSQL indexes lean : comment gérer la fragmentation des index ;
- `IN is not the same as ANY <https://kmoppel.github.io/2023-07-04-til-in-is-not-the-same-as-any/>`_
  les deux opérateurs produisent le même résultat, mais pas de la même manière ;
- `Ora2Pg 24.0 <https://github.com/darold/ora2pg/releases/tag/v24.0>`_
  have been released : comme nouveauté, notons le support de MS-SQL-Server !
- `Subqueries and performance in PostgreSQL <https://www.cybertec-postgresql.com/en/subqueries-and-performance-in-postgresql/>`_
  de l'utilisation des sous-requêtes et des CTEs ; une matrice des fonctionnalités
  de PostgreSQL générée à l'aide de ChatGPT ;
- `Citus 12: Schema-based sharding for PostgreSQL <https://www.citusdata.com/blog/2023/07/18/citus-12-schema-based-sharding-for-postgres/>`_
  Citus est un outil pour faire grandir votre architecture de base de données ;
- `How To Measure the Network Impact on PostgreSQL Performance <https://www.percona.com/blog/how-to-measure-the-network-impact-on-postgresql-performance/>`_
  cet aspect de l'analyse de la performance est trop souvent ignoré, et pourtant,
  il est important ;


2023-07-22 **10 Postgres tips for beginners** by Nikolay Samokhvalov
==========================================================================

- https://postgres.ai/blog/20230722-10-postgres-tips-for-beginners
- https://postgres.fm/
- https://postgres.fm/episodes/index-maintenance
- https://postgres.ai/docs/tutorials/database-lab-tutorial

Getting started with PostgreSQL can be both exciting and challenging.
It's more than just another database—it's a system packed with features
that can change how you handle data.

Every Friday, Michael Christofides (pgMustard) and I discuss these features
on our podcast, `Postgres.FM <https://postgres.fm/>`_ (there is also a video version on YouTube).

We've been at it for 55 weeks straight since July 2022, and we're not
stopping anytime soon.

Our latest episode was all about helping newcomers to PostgreSQL


10. Index maintenance: a necessary practice
-------------------------------------------------

- https://postgres.fm/episodes/index-maintenance

Indexes are pivotal for performance in any relational database, and
Postgres is no exception.

Why it's important
++++++++++++++++++++

Over time, as data changes, indexes become fragmented and less efficient.
Even with modern Postgres versions (especially with btree optimization
Postgres 13 and 14 received) and a well-tuned autovacuum, index health
still declines over time, while numerious writes are happening.

Declining health of indexes
++++++++++++++++++++++++++++++++

When data is inserted, updated, or deleted, the indexes reflecting this
data undergo changes.
These changes can cause the index structure to become unbalanced or have
dead entries, slowing down search performance.

Rebuilding indexes
++++++++++++++++++++

Unlike some misconceptions, indexes don't maintain their optimal structure
indefinitely.
Periodically, they need to be rebuilt. This process involves creating a
fresh version of the index, which often results in a more compact and
efficient structure.

Preparing for these rebuilds, preferably in an automated manner, ensures
that your database performance remains consistent.

Cleanup
++++++++

Besides rebuilding, it's equally crucial to remove unused or redundant
indexes. They not only waste storage space but can also slow down write
operations. Regularly reviewing and cleaning up unnecessary indexes
should be part of your maintenance routine.

To reiterate a crucial point: indexes are vital, but, like all tools,
they require maintenance.
Keeping them in good health is essential for maintaining the swift
performance of your Postgres database.

Podcast episode about this:

- `PostgresFM e012: Index maintenance <https://postgres.fm/episodes/index-maintenance>`_


Bonus tip: Postgres documentation and source comments are your trustworthy companions
-----------------------------------------------------------------------------------------

Michael emphasized the unmatched value of official documentation.
Whether you're just starting with Postgres or have been working with it
for a while, always have the official Postgres documentation bookmarked.

Here's why:

- Comprehensive and up-to-Date: Official documentation is the most
  authoritative source of information, detailing every feature, behavior,
  and nuance of Postgres.
- Release notes: Every time a new version of Postgres is released, the
  release notes provide a concise summary of all changes, new features,
  and improvements. It's a great way to keep up with what's new and what
  might impact your existing setups.
- Source code comments/READMEs: For those who like to dive deep, the source
  code of Postgres is not only a learning resource but also a reference.
  Comments and readme files embedded within the code can offer insights
  and explanations that might not be evident elsewhere.


2023-07-19 mayan-edms #1148: Upgrade Documentation: need to include db name
=======================================================================================

- https://gitlab.com/mayan-edms/mayan-edms/-/issues/1148

In Step 11 of the Upgrade between major versions section of the Upgrade
Documentation, the DB name is not included in the command line to restore
from SQL dump. With the default installation settings, this will fail.

psql expects the default database to be postgres, whereas on a default
installation of MayanEDMS, the db name is mayan_db. (At least, I believe it is)

If you don't include the db name in the command, you'll get an unhelpful message like::

    psql: error: FATAL:  database "mayan_user" does not exist

So the command line should be::

    cat mayan.sql | docker-compose --file docker-compose.yml --project-name mayan exec -T postgresql /bin/bash -c 'psql --username="$POSTGRES_USER" --dbname="$POSTGRES_DB"'
