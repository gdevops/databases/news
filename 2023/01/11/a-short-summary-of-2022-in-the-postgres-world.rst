
.. _postgres_news_for_2022_2023_01_11:

=================================================================
2023-01-11 **A Short Summary of 2022 in the Postgres World**
=================================================================

- https://www.migops.com/blog/a-short-summary-of-the-year-2022-in-the-postgresql-world/


DB-Engines Rankings - PostgreSQL is among the top 3 databases for the Year 2022
================================================================================

DB-Engines database popularity ranking for the Years - 2017, 2018 and 2020
had ranked PostgreSQL as the DBMS of the Year.

In the year 2021, PostgreSQL ranked as the runner-up while Snowflake
ranked first.

For the year 2022, Snowflake continued to be the first with Google BigQuery
being the second and PostgreSQL being the third in rankings.

This greatly shows the popularity of PostgreSQL being better than relational
databases like MySQL, Oracle and SQL Server for the year 2022.


Extensions that took birth in 2022
=======================================

PostgreSQL extensions are one of the major reasons for increased migrations
from Oracle to PostgreSQL and SQL Server to PostgreSQL. Numerous developers
across the world have contributed their patches or ideas that resulted
in the birth of many new extensions in 2022.

Following are some of the new extensions that are released in 2022.

- pg_ivm : The pg_ivm module provides Incremental View Maintenance (IVM)
  of Materialized Views in PostgreSQL.
  It can updated materialized views more efficiently when compared to
  the recomputing required through REFRESH MATERIALIZED VIEW. 
- PGSpider : PGSpider Extension(pgspider_ext) is an extension to
  construct High-Performance SQL Cluster Engine for distributed big data.
  pgspider_ext enables PostgreSQL to access a number of data sources
  using Foreign Data Wrapper(FDW) and retrieves the distributed data source
  vertically.
- PLHaskell : This project is a "procedural language" extension of
  PostgreSQL allowing the execution of code in Haskell within SQL code.
- rapidrows : RapidRows is an open-source, zero-dependency, single-binary
  API server that can be configured to run SQL queries, perform scheduled
  jobs and forward PostgreSQL notifications to websockets.
- pg_enquo : A PostgreSQL extension to provide Encrypted Query Operations
  (enquo).
- pg_show_rewritten_query : Display a query as it will be executed, that
  is after the analyze and rewrite steps (PostgreSQL 15+).
- oracle_fnd : Functions that emulate the FND_GLOBAL and FND_PROFILE
  package's API using custom variables. (Announced by MigOps).
- pg_wkhtmltopdf : PostgreSQL implementation of Convert HTML to PDF
  using Webkit (QtWebKit).
- pg_rowalesce : The pg_rowalesce PostgreSQL extensions its defining
  feature is the rowalesce() function. rowalesce() is like coalesce(),
  but for rows and other composite types. From its arbitrary number of
  argument rows, for each field/column, rowalesce() takes the value
  from the first row for which that particular field/column has a not null value.
- pg_mupdf : PostgreSQL implementation of Convert HTML to PDF using MuPDF.*
- pg_mockable : The pg_mockable PostgreSQL extension can be used to create
  mockable versions of functions from other schemas.
- pg_migrate : pg_migrate is a PostgreSQL extension and CLI which lets
  you make schema changes to tables and indexes.
  Unlike ALTER TABLE it works online, without holding a long lived exclusive
  lock on the processed tables during the migration. It builds a copy
  of the target table and swaps them.
- pg_injection : PostgreSQL detection of sql injection.
- pg_icu_parser : PostgreSQL text search parser using ICU boundary analysis.
- pg_diffix : pg_diffix is a PostgreSQL extension for strong dynamic anonymization.
  It ensures that answers to simple SQL queries are anonymous.
  For more information, visit the Open Diffix website.
- notify_now : This simple extension allows you to return multiple
  responses from a single query using the built-in PostgreSQL NOTIFY API.
- lower_quantile : An extension to calculate lower quantile aggregates.

Conclusion
============

The popularity of PostgreSQL and its adoption has been increasing year
over year.

At MigOps, we have experienced a 300% growth compared to 2021 in the number
of customers migrating from Oracle to PostgreSQL.

We also started customers showing interests in migrations from SQL to PostgreSQL,
Informix to PostgreSQL and DB2 to PostgreSQL.

We also noticed increased requests for Migration Assessments and Database
Migration support in 2022.
Leadership teams in larger organizations are considering cost savings
using (1) Migration to PostgreSQL and paying zero license fees with better
features supporting enterprises and (2) Migrations to cloud to reduce
infrastructure maintenance costs.

MigOps is glad to have a team to support you with migrations to PostgreSQL
and cloud. Contact Us today to see how we can simplify your database migrations
