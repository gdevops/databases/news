.. index::
   pair: Haki Benita ; How to Get or Create in PostgreSQL And why it is so easy to get wrong (2024-08-05)

.. _benita_2024_08_05:

============================================================================================================
2024-08-05 **How to Get or Create in PostgreSQL And why it is so easy to get wrong...** by Haki Benita
============================================================================================================

- https://hakibenita.com/postgresql-get-or-create
- https://hakibenita.com/feeds/all.atom.xml
- https://github.com/django/django/blob/a190c03afec0feef27e097727569ba064b97ac7a/django/db/models/query.py#L938-L961
- https://hakibenita.com/django-concurrency (Handling Concurrency Without Locks How to not let concurrency cripple your system, 2022-06-09)
- https://docs.djangoproject.com/en/dev/topics/db/transactions/#handling-exceptions-within-postgresql-transactions


Introduction
================

"Get or create" is a very common operation for syncing data in the database, 
but implementing it correctly may be trickier than you may expect. 

If you ever had to implement it in a real system with real-life load, 
you may have overlooked potential race conditions, concurrency issues and 
even bloat !

In this article I explore ways to "get ot create" in PostgresSQL.
