
.. _postgresql_2024_05_29:

===========================================================
2024-05-23 **PostgreSQL 17 Beta 1 Released !** par Dalibo
===========================================================

- https://www.postgresql.org/about/news/postgresql-17-beta-1-released-2865/
- https://blog.dalibo.com/2024/05/28/pg17beta1.html


La semaine dernière, le PostgreSQL Global Development Group a annoncé la sortie
de la première version bêta de la version majeure 17.

Jetons un œil sur ce que contient cette nouvelle version.

Les nouveautés
================

Parmi les nouveautés attendues sur les performances, notons des améliorations
sur les opérations de maintenance (VACUUM et ANALYZE).

Les requêtes utilisant des CTE sont optimisées.

Les index B-Tree et BRIN profitent aussi de belles améliorations, les premiers
lorsqu’ils sont utilisés pour des clauses IN, les seconds pour des constructions
parallélisées.

Les contraintes NOT NULL sont mieux prises en compte pour optimiser l’exécution
des requêtes.
