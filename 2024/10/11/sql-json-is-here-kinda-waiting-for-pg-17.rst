

.. _sql_json_2024_10_11:

==================================================================
2024-10-11 **SQL/JSON is here! (kinda “Waiting for Pg 17”)**
==================================================================

- https://www.depesz.com/2024/10/11/sql-json-is-here-kinda-waiting-for-pg-17/


Amazing. Awesome. Well, but what is it? 
We could store json data in Pg since PostgreSQL 9.2 – so it's been there 
for over 12 years now. 

How is the new shiny thing different ? What does it allow you to do?

Let's see if I can shed some light on it…

For starters: SQL/JSON is a standard. 
As in: written down by people not bound by “only for PostgreSQL" rules. 
This means that whatever is here will (eventually? hopefully?) work in 
other databases, once they will implement it.

We actually had parts of it in PostgreSQL for some time. J
SON datatype, JSON PATH expressions. But now, we get MORE.

Up to this moment, we (generally speaking) had json and jsonb datatypes, 
some functions and operators (including the ones that used JSONPATH datatype). 

This all doesn't go away. But we get more stuff. Some of it is kinda redundant 
to what we could have done previously, but it is there now, in this new way, 
because it is part of the standard. 

Some of it wasn't possible earlier or, at the very least, wasn't simple earlier.
