

.. _postgresql_16-2:

===============================================================================================================================================
2024-02-09 **Sortie de PostgreSQL 16.2**, 15.6, 14.11, 13.14 et 12.18 par par Christophe Courtois, Pierrick Chovelon, David Bidoc, Laura Ricci
===============================================================================================================================================

- https://blog.dalibo.com/2024/02/09/postgresql_release.html
- https://www.postgresql.org/about/news/postgresql-162-156-1411-1314-and-1218-released-2807/
- https://www.postgresql.org/docs/release/16.2/

Description
===============

Le PostgreSQL Global Development Group a `publié <https://www.postgresql.org/about/news/postgresql-162-156-1411-1314-and-1218-released-2807/>`
une mise à jour pour toutes les versions supportées de PostgreSQL, c’est à dire les versions 16.2,
15.6, 14.11, 13.14 et 12.18.

Cette version corrige une faille de sécurité et plus de 65 bugs signalés
au cours des derniers mois. Voici notre traduction.


Si vous utilisez des index GIN, il se peut que vous deviez les réindexer
après la mise à jour. Veuillez consulter les notes de mise à jour pour
plus d’informations.

Pour obtenir la liste complète des changements, veuillez consulter les
`notes de mise à jour <https://www.postgresql.org/docs/release/16.2/>`_.
