.. index::
   pair: no-relational ; failure

.. _no_sql_relational_failure:

====================================================================================================
Building relational #databases with non relational #NoSQL is just repeating a **very old failure**
====================================================================================================

.. seealso::

   - https://x.com/sjstoelting/status/1248866410601885697?s=20
   

.. figure:: tweet_sjstoelting.png
   :align: center

Building relational #databases with non relational #NoSQL is just repeating 
a **very old failure**.

Relational databases have been invented because document oriented databases 
failed to do the job.

**That already happened early 1970s**.

.. warning:: It's like these people haven't learned anything.
