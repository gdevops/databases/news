

========================================================================
Les manuels de formation Dalibo Publié le 27/01/2022 par Laura Ricci
========================================================================

- https://blog.dalibo.com/2022/01/27/maj_manuels.html

Les manuels Dalibo
=====================

PostgreSQL et ses logiciels satellites évoluent en permanence, en témoignent
les annonces régulières des releases.
C’est pourquoi nos manuels, sur lesquels se basent nos formations, sont
mis à jour quatre fois par an.

Au-delà de l’intégration des nouveautés des versions majeures du SGBDR
communautaire, il s’agit d’un véritable travail de fond, continu, module
par module.

PostgreSQL est si riche que des choix s’imposent à propos du contenu,
discutés par les DBA de la société, par ailleurs formateurs et contributeurs
actifs à la communauté PostgreSQL.



Un nouveau manuel pour la HA
===============================

L’un de nos DBA formateurs, Franck Boudehen, a perçu des nouveaux besoins
autour de la haute disponibilité, puis a mis progressivement en place un
atelier sur ce sujet.

Voici le manuel correspondant à cette nouvelle formation :

HAPAT : Haute disponibilité avec PostgreSQL. `PDF <https://dali.bo/hapat_pdf>`_, epub, html, slides



La version 22.01
====================

Cette dernière version prend surtout en compte la dernière mise à jour
majeure, PostgreSQL 14, ainsi que la mise à jour de nombreux outils tels
que pgBackRest.

Un grand merci aux contributeurs et relecteurs de Dalibo !

Retrouvez le sommaire des formations Dalibo et de leurs modules `ici <https://dali.bo/sommaire_html>`_.

Si vous notez des éléments qui demandent à être clarifiés, précisés,
corrigés… envoyez-nous vos suggestions !

La prochaine release est planifiée au printemps 2022.
