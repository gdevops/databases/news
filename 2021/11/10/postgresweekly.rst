
.. _postgresweekly_2021_11_10:

==============================================
Postgres Weekly #​430 — November 10, 2021
==============================================

- https://postgresweekly.com/issues/430


Lots of Lesser Known Postgres Features by Haki
=================================================

- https://hakibenita.com/postgresql-unknown-features

Haki’s articles are often catnip to us as they cover a lot of ground,
and so it goes with this roundup of practical examples of Postgres
features you might not be familiar with.

Worth a skim of the Table of Contents at the very least!


Three Cases Against Using IF NOT EXISTS / IF EXISTS by Nikolay
=================================================================

Three Cases Against Using IF NOT EXISTS / IF EXISTS

Many DDL statements accept IF (NOT) EXISTS modifiers to let statements
only run under certain conditions, but overuse “may lead to negative consequences”
argues Nikolay.
